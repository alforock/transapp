import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { PlanificacionPage } from '../planificacion/planificacion';
import { ServicioDatosProvider } from '../../providers/servicio-datos/servicio-datos';

import { Linea, Estacion, Trayecto } from '../../models/modelos';

/**
 * Generated class for the TrayectosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-trayectos',
  templateUrl: 'trayectos.html',
})
export class TrayectosPage {

  trayectos: any;
  error: string;

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public navParams: NavParams,
              public servicioDatos: ServicioDatosProvider) {
  }

  ionViewDidLoad() {
    this.obtenerDatos();
  }

  obtenerDatos(){
    let loading = this.loadingCtrl.create({
      content: 'Buscando, espere...'
    });
    loading.present();

    this.servicioDatos.getTrayectos()
      .then(data =>{
          this.extraeTrayectos(data).then(trayectos => {
            this.trayectos = trayectos;
          });
        },(err) => {
          console.log("error al obtener los trayectos", err);
          this.error = err;
        }
      ).then(() => {
        loading.dismiss();
      }).catch(e => {
        console.log("error al obtener los trayectos", e);
      });

  }

  extraeTrayectos(data: any) {

    var trayectosTmp: Trayecto[] = new Array();
    return new Promise(resolve => {
      data.forEach(trayecto => {

        var newTrayecto = new Trayecto();

        //Linea Inicio
        var newLineaIni = new Linea();
        newLineaIni.id = trayecto.initial_station.line.id;
        newLineaIni.name = trayecto.initial_station.line.name;
        newLineaIni.color = trayecto.initial_station.line.color;
        newTrayecto.lineaInicio = newLineaIni;

        //Estacion Inicio
        var newEstacionIni = new Estacion();
        newEstacionIni.id = trayecto.initial_station.id;
        newEstacionIni.name = trayecto.initial_station.station.name;
        newEstacionIni.colorServicioExpress = (trayecto.initial_station.express_service?
                                        trayecto.initial_station.express_service.color : '');
        newTrayecto.estacionInicio = newEstacionIni;

        //Linea Fin
        var newLineaFin = new Linea();
        newLineaFin.id = trayecto.end_station.line.id;
        newLineaFin.name = trayecto.end_station.line.name;
        newLineaFin.color = trayecto.end_station.line.color;
        newTrayecto.lineaFinal = newLineaFin;

        //Estacion Fin
        var newEstacionFin = new Estacion();
        newEstacionFin.id = trayecto.end_station.id;
        newEstacionFin.name = trayecto.end_station.station.name;
        newEstacionFin.colorServicioExpress = (trayecto.end_station.express_service?
                                        trayecto.end_station.express_service.color : '');
        newTrayecto.estacionFinal = newEstacionFin;

        trayectosTmp.push(newTrayecto);
      });

      resolve(trayectosTmp);
    });
    
  }

  agregarTrayecto() {
    this.navCtrl.push(PlanificacionPage);
  }

  verificaRutasTrayecto(trayecto: Trayecto) {

    console.log("Enviando: ", trayecto);
    this.navCtrl.push(PlanificacionPage, {
      lineaIni: trayecto.lineaInicio,
      estacionIni: trayecto.estacionInicio,
      lineaFin: trayecto.lineaFinal,
      estacionFin: trayecto.estacionFinal
    });
  }

  eliminarTrayecto() {
    
  }



}
