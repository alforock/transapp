import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { ServicioAutenticacionProvider } from '../../providers/servicio-autenticacion/servicio-autenticacion';

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  loading: Loading;
  createSuccess = false;

  usuario: string;
  password: string;
  email: string;

  constructor(private loadingCtrl: LoadingController,
              private alertCtrl: AlertController, 
              public navCtrl: NavController, 
              public autenticacion: ServicioAutenticacionProvider, 
              public navParams: NavParams) {
  }

  public register() {

      this.showLoading();
    
      this.autenticacion.registro(this.usuario, this.password, this.email)
        .then(
          success => {
            console.log(success);
            
            if (success) {
              this.createSuccess = true;
              this.showPopup("Éxito!", "Cuenta creada con éxito");
            } else {
              this.showPopup("Error!", "Error al crear la cuenta");
            }
          },
          error => {
            if(error.json()){
              error = error.json();
              var keys = Object.keys(error);
              this.showError(error[keys[0]]);
            }else{
              if(error.name && error.name === 'TimeoutError'){
                this.showError('El servidor no responde');
              }else{
                this.showError(error);
              }
            }
          }
        ).then(() => {
          this.navCtrl.pop();
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Espere un momento...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.navCtrl.popToRoot();
            }
          }
        }
      ]
    });
    alert.present();
  }
  

}
