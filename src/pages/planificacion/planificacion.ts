import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';

import { ServicioPlanificacionProvider } from '../../providers/servicio-planificacion/servicio-planificacion';
import { ServicioDatosProvider } from '../../providers/servicio-datos/servicio-datos';
import { ServicioSensoresProvider } from '../../providers/servicio-sensores/servicio-sensores';

import { EvaluacionPage } from '../evaluacion/evaluacion';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Linea, Estacion, Ruta, Tramo, Transbordo } from '../../models/modelos';

/**
 * Generated class for the PlanificacionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-planificacion',
  templateUrl: 'planificacion.html',
})
export class PlanificacionPage {

  /*lineas: any;
  lineasEstaciones: any;
  tramo: any;*/
  error: string;

  /*lineaIni: Linea;
  lineaFin: Linea;*/

  estacionesInicioSeleccionadas: Array<Estacion>;
  estacionesFinSeleccionadas: Array<Estacion>;

  rutasObtenidas: any;

  formularioPlanificacion: FormGroup;

  lineaInicio: Linea;
  estacionInicio: Estacion;
  lineaFinal: Linea;
  estacionFinal: Estacion;

  constructor(public diagnostico: Diagnostic,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public servicioPlanificacion: ServicioPlanificacionProvider,
    public servicioDatos: ServicioDatosProvider,
    public servicioSensores: ServicioSensoresProvider,
    public formBuilder: FormBuilder,
    public toast: ToastController,
    private alertCtrl: AlertController) {

    this.lineaInicio = this.navParams.get('lineaIni');
    this.estacionInicio = this.navParams.get('estacionIni');
    this.lineaFinal = this.navParams.get('lineaFin');
    this.estacionFinal = this.navParams.get('estacionFin');

    this.formularioPlanificacion = this.formBuilder.group({
      lineaIni: [''],
      estacionIni: ['', Validators.required],
      lineaFin: [''],
      estacionFin: ['', Validators.required]
    });

  }

  ionViewWillEnter() {
    this.obtenerDatos();
    this.preFillForm();
  }

  obtenerDatos() {
    let loading1 = this.loadingCtrl.create({
      content: 'Cargando datos, espere...'
    });
    loading1.present();

    this.servicioDatos.getEstaciones()
      .then(() => {
        this.servicioDatos.getLineas()
          .then(data => {
            //this.lineas = data;
            console.log("Datos de lineas obtenido", data);
            this.presentToast("Datos de líneas y estaciones obtenidos","bottom");
            loading1.dismiss();

            /** 
              * Si está funcionando el gps, obtiene la ubicación para dar la estación inicial
              */
            if (!this.lineaInicio && this.servicioSensores.activado.gps) {
              console.log("Servicio de ubicacion activo, se busca la estacion más cercana");
              this.presentToast("Servicio de ubicacion activo, se busca la estacion más cercana","bottom");

              let loading = this.loadingCtrl.create({
                content: 'Buscando estación más cercana, espere...'
              });
              loading.present();

              this.servicioSensores.getPosicionActual().then(ubicacion => {
                console.log(ubicacion);

                this.servicioPlanificacion.getNearStation(ubicacion.coords.latitude, ubicacion.coords.longitude).then((estacionMasCercana: any) => {

                  this.servicioDatos.getLinea(estacionMasCercana.station_line.line).then(linea => {
                    var lineaResultante = linea;
                    this.estacionesInicioSeleccionadas = lineaResultante.estaciones;

                    this.servicioDatos.getEstacion(estacionMasCercana.station_line.id).then(estacion => {
                      var estacionResultante = estacion;

                      this.formularioPlanificacion.setValue({
                        lineaIni: lineaResultante,
                        estacionIni: estacionResultante,
                        lineaFin: '',
                        estacionFin: ''
                      });

                      loading.dismiss();
                    });
                  });

                }, e => {
                  this.presentToast("Error al obtener la estación más cercana","bottom");
                  loading.dismiss();
                  console.log("Error al obtener la estacion más cercana: ", e);
                }).catch(this.loguearExcepcion);

              }, e => {
                loading.dismiss();
                console.log("Error al obtener la ubicación actual: ", e);
              }).catch(this.loguearExcepcion);


            }

          }, (err) => {
            console.log(err);
            this.error = err;
          });
      })
      .catch((excepcion) => {
        this.error = excepcion;
        console.log(excepcion);
      });
  }

  preFillForm() {

    if (this.lineaInicio) {
      new Promise(resolve => {
        this.servicioDatos.getLinea(this.lineaInicio.id).then((linea: Linea) => {
          this.lineaInicio = linea;
        }).then(() => {
          this.servicioDatos.getEstacion(this.estacionInicio.id).then((estacion: Estacion) => {
            this.estacionInicio = estacion;
          }).then(() => {
            this.servicioDatos.getLinea(this.lineaFinal.id).then((linea: Linea) => {
              this.lineaFinal = linea;
            });
          }).then(() => {
            this.servicioDatos.getEstacion(this.estacionFinal.id).then((estacion: Estacion) => {
              this.estacionFinal = estacion;
              resolve(true);
            });
          });
        });
      }).then(() => {
        this.estacionesInicioSeleccionadas = this.lineaInicio.estaciones;
        this.estacionesFinSeleccionadas = this.lineaFinal.estaciones;

        this.formularioPlanificacion.setValue({
          lineaIni: this.lineaInicio,
          estacionIni: this.estacionInicio,
          lineaFin: this.lineaFinal,
          estacionFin: this.estacionFinal
        });
      });

    }
  }

  enviarForm() {

    let loading = this.loadingCtrl.create({
      content: 'Buscando rutas...'
    });
    loading.present();

    var rutaExpress = this.evaluaHorarioExpress();
    console.log("Obtiene ruta express? ", rutaExpress);

    if (rutaExpress) {
      this.servicioPlanificacion.getExpressPath(this.formularioPlanificacion.value.estacionIni.id, this.formularioPlanificacion.value.estacionFin.id)
        .then(data => {
          this.extraeRutas(data).then((rutas) => {
            this.rutasObtenidas = rutas;
            this.presentToast("Se obtuvieron correctamente las rutas","bottom");
            //this.muestraAlerta("¡Rutas obtenidas!", "Se obtuvieron correctamente las rutas");
            console.log("RUTAS: ", this.rutasObtenidas);
          })
        }, (err) => {
          //this.muestraAlerta("Error", "Ocurrió un error al obtener las rutas, por favor intenta más tarde");
          this.presentToast("Ocurrió un error al obtener las rutas, por favor intenta más tarde","bottom");
          console.log("Error al obtener el path", err);
          this.error = err;
        }
        ).then(() => {
          loading.dismiss();
        }).catch(this.loguearExcepcion);
    } else {
      this.servicioPlanificacion.getPath(this.formularioPlanificacion.value.estacionIni.id, this.formularioPlanificacion.value.estacionFin.id)
        .then(data => {
          this.extraeRutas(data).then((rutas) => {
            this.rutasObtenidas = rutas;
            this.presentToast("Se obtuvieron correctamente las rutas","bottom");
            //this.muestraAlerta("¡Rutas obtenidas!", "Se obtuvieron correctamente las rutas");
            console.log("RUTAS: ", this.rutasObtenidas);
          })
        }, (err) => {
          this.presentToast("Ocurrió un error al obtener las rutas, por favor intenta más tarde","bottom");
          //this.muestraAlerta("Error", "Ocurrió un error al obtener las rutas, por favor intenta más tarde");
          console.log("Error al obtener el path", err);
          this.error = err;
        }
        ).then(() => {
          loading.dismiss();
        }).catch(this.loguearExcepcion);
    }


  }

  guardarTrayecto() {

    let loading = this.loadingCtrl.create({
      content: 'Guardando trayecto...'
    });
    loading.present();

    this.servicioDatos.guardarTrayecto(this.formularioPlanificacion.value.estacionIni.id, this.formularioPlanificacion.value.estacionFin.id)
      .then(data => {
        this.presentToast("Trayecto almacenado exitosamente!","bottom");
        //this.muestraAlerta("Guarda la ruta", "Trayecto almacenado exitosamente!");
        console.log("Trayecto almacenado!");
      }, (err) => {
        this.presentToast("Error al almacenar el trayecto","bottom");
        //this.muestraAlerta("Error", "Error al almacenar el trayecto");
        console.log("Error al guardar el trayecto", err);

      })
      .then(() => {
        loading.dismiss();
      }).catch(this.loguearExcepcion);

    if (this.navCtrl.canGoBack) {
      this.navCtrl.pop().catch(e => { });
    }
  }

  seleccionaRuta(rutaAPasar: Ruta) {
    console.log("Ruta a pasar: ", rutaAPasar);
    this.navCtrl.push(EvaluacionPage, {
      ruta: rutaAPasar
    });
  }

  setValoresEstacionesIni(linea: Linea) {
    this.estacionesInicioSeleccionadas = linea.estaciones;
  }

  setValoresEstacionesFin(linea: Linea) {
    this.estacionesFinSeleccionadas = linea.estaciones;
  }

  loguearExcepcion(excep) {
    console.log("Error: ", excep);
  }

  extraeRutas(rutas: any) {

    var rutaTmp: Ruta[] = new Array();
    var contador = 0;
    var maxRutas = 3;
    var BreakException = {};
    return new Promise(resolve => {

      try {
        rutas.forEach((ruta: any) => {

          if (contador === maxRutas) {
            throw BreakException;
          }

          var newRuta = new Ruta();

          /*ruta.trans.forEach((transbordo) => {
            var newTransbordo = new Transbordo();
  
            this.servicioDatos.getEstacion(transbordo[0].id).then((data: Estacion) => {
              newTransbordo.estacionDesde = data;
            }).then(() => {
              this.servicioDatos.getLinea(transbordo[1].id).then((data: Linea) => {
                newTransbordo.lineaHasta = data;
              }).then(() => {
                newRuta.transbordos.push(newTransbordo);
              });
            }).catch(e => {
              console.error("Error al obtener estacion",transbordo[0].id);
            });           
    
          });*/

          ruta.stretchs.forEach((tramo) => {
            var newTramo = new Tramo();

            newTramo.id = tramo.id;

            this.servicioDatos.getEstacion(tramo.start.id).then((data: Estacion) => {
              newTramo.inicio = data;
            }).then(() => {
              this.servicioDatos.getEstacion(tramo.end.id).then((data: Estacion) => {
                newTramo.fin = data;
              }).then(() => {
                newRuta.tramos.push(newTramo);
              });
            }).catch(e => {
              console.error("Error al obtener estacion. Tramo -> ", tramo);
            });

            if (tramo.id == null) {
              var newTransbordo = new Transbordo();

              this.servicioDatos.getEstacion(tramo.start.id).then((data: Estacion) => {
                newTransbordo.estacionDesde = data;
              }).then(() => {
                this.servicioDatos.getLinea(newTransbordo.estacionDesde.idLinea).then((data: Linea) => {
                  newTransbordo.lineaDesde = data;
                }).then(() => {
                  this.servicioDatos.getEstacion(tramo.end.id).then((data: Estacion) => {
                    newTransbordo.estacionHasta = data;
                  }).then(() => {
                    this.servicioDatos.getLinea(newTransbordo.estacionHasta.idLinea).then((data: Linea) => {
                      newTransbordo.lineaHasta = data;
                    }).then(() => {
                      newRuta.transbordos.push(newTransbordo);
                    });
                  });
                });

              });
            }

          });

          rutaTmp.push(newRuta);
          contador++;

        });
      } catch (error) {
        console.log("ERROR AL CARGAR RUTAS:", error, rutaTmp);
      }

      resolve(rutaTmp);
    });

  }

  evaluaHorarioExpress() {
    var d = new Date();
    var diaSemana = d.getDay();
    var horas = d.getHours();
    var minutos = d.getMinutes();

    if (diaSemana > 0 && diaSemana < 6) {
      var horaMin = parseInt(horas + "" + minutos);
      return (this.servicioPlanificacion.horariosExpress.am.inicio <= horaMin && this.servicioPlanificacion.horariosExpress.am.fin <= horaMin) ||
        (this.servicioPlanificacion.horariosExpress.pm.inicio <= horaMin && this.servicioPlanificacion.horariosExpress.pm.fin <= horaMin);
    } else {
      return false;
    }


  }

  public muestraAlerta(titulo, subtitulo) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: subtitulo,
      buttons: ['Ok']
    });
    alert.present();
  }

  presentToast(mensaje, posicion) {
    let toastNuevo = this.toast.create({
      message: mensaje,
      duration: 2000,
      position: posicion,
      dismissOnPageChange: true
    });
  
    toastNuevo.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toastNuevo.present();
  }

}
