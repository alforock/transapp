import { Component } from '@angular/core';
import { IonicPage, Slides, NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { ViewChild } from '@angular/core';

import { Ruta, Tramo, TramoEvaluacion, Transbordo, TransbordoEvaluacion, Evaluacion } from '../../models/modelos';
import { ServicioSensoresProvider } from '../../providers/servicio-sensores/servicio-sensores';
import { ServicioDatosProvider } from '../../providers/servicio-datos/servicio-datos';

import { SimpleTimer } from 'ng2-simple-timer';

//import { Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the EvaluacionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-evaluacion',
  templateUrl: 'evaluacion.html',
})
export class EvaluacionPage {

  @ViewChild(Slides) slides: Slides;

  ruta: Ruta;
  evaluacion: Evaluacion;

  //Conteo de tiempo
  tiempo: number = 1511751600000;
  timerId: string;

  tramo: Tramo;

  //Aspectos de evaluacion del usuario
  valoracionGeneral: any;
  congestion: any;
  frenazos: any;

  viajeIniciado: boolean;

  constructor(private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public servicioDatos: ServicioDatosProvider,
    public servicioSensores: ServicioSensoresProvider,
    public toast: ToastController,
    private alertCtrl: AlertController,
    public timer: SimpleTimer) {
    this.ruta = this.navParams.get('ruta');

    this.viajeIniciado = false;
    this.timer.newTimer('1seg', 1);

    if (!this.evaluacion) {
      this.evaluacion = this.iniciaEvaluacion(this.ruta);
    }
  }

  cambiaSlide() {

    var indiceActual = this.slides.getActiveIndex();
    var indiceAnterior = this.slides.getPreviousIndex();

    //this.slides.lockSwipeToPrev(true);

    console.log("indiceActual", indiceActual);
    console.log("indiceAnterior: ", indiceAnterior);

    var largoSlides = this.slides.length();

    if (indiceAnterior < largoSlides) {
      if (indiceAnterior == 0) {
        this.agregaEvaluacionInicio(this.evaluacion, this.congestion);
      } else {
        if (this.ruta.tramos[indiceAnterior].id == null) {
          this.finalizaEvaluacionTransbordo(this.ruta.tramos[indiceAnterior], this.evaluacion, this.congestion);
        } else {
          this.finalizaEvaluacionTramo(this.ruta.tramos[indiceAnterior], this.evaluacion, this.valoracionGeneral, this.congestion, this.frenazos);
        }
      }

      if (indiceActual < largoSlides) {
        if (this.ruta.tramos[indiceActual].id == null) {
          this.iniciaEvaluacionTransbordo(this.ruta.tramos[indiceActual], this.evaluacion);
        } else {
          this.iniciaEvaluacionTramo(this.ruta.tramos[indiceActual], this.evaluacion);
        }
      }

    }

    this.frenazos = null;
    this.congestion = null;
    this.valoracionGeneral = null;

  }

  clickFinalizaRuta() {
    let alert = this.alertCtrl.create({
      title: 'Finalizar evaluación',
      message: '¿Está seguro que desea finalizar la evaluación?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.finalizaRuta();
          }
        }
      ]
    });
    alert.present();
  }

  finalizaRuta() {
    var indiceActual = this.slides.getActiveIndex();
    var largoSlides = this.slides.length();

    console.log("indiceActual", indiceActual, this.ruta.tramos[indiceActual]);

    if (indiceActual < largoSlides) {
      if (indiceActual == 0) {
        this.agregaEvaluacionInicio(this.evaluacion, this.congestion);
      } else {
        if (this.ruta.tramos[indiceActual].id == null) {
          this.finalizaEvaluacionTransbordo(this.ruta.tramos[indiceActual], this.evaluacion, this.congestion);
        } else {
          this.finalizaEvaluacionTramo(this.ruta.tramos[indiceActual], this.evaluacion, this.valoracionGeneral, this.congestion, this.frenazos);
        }
      }

    }

    this.finalizaEvaluacionRuta(this.ruta, this.evaluacion);

    this.enviarForm();
    this.viajeIniciado = false;

    this.frenazos = null;
    this.congestion = null;
    this.valoracionGeneral = null;

  }

  ionViewDidLoad() {
    console.log('EvaluacionPage cargada');
  }


  enviarForm() {
    this.servicioDatos.enviaEvaluacion(this.evaluacion);
    console.log("Enviando evaluacion: ", this.evaluacion);
  }

  iniciaEvaluacion(ruta: Ruta) {
    var tramo = ruta.tramos[0];

    var evaluacion = new Evaluacion();
    evaluacion.start.station = tramo.inicio.id;
    evaluacion.start.datetime = this.customISOstring(new Date());

    evaluacion.accelerometer_frequency = 3;
    evaluacion.gyroscope_frequency = 3;

    return evaluacion;
  }

  iniciaViaje() {
    this.viajeIniciado = true;
    
    try {
      this.servicioSensores.iniciaPosicion();
      this.servicioSensores.iniciaGiroscopio();
      this.servicioSensores.iniciaAcelerometro();
    } catch (error) {
      
    }
    this.timerId = this.timer.subscribe("1seg", () => this.tiempo+=1000);
  }

  agregaEvaluacionInicio(evaluacion: Evaluacion, congestion: number) {
    if(congestion == null || congestion == undefined){
      congestion = 0;
    }
    evaluacion.start.congestion = congestion;
  }

  iniciaEvaluacionTramo(tramo: Tramo, evaluacion: Evaluacion) {
    var tramoEvaluacion;

    tramoEvaluacion = new TramoEvaluacion();
    tramoEvaluacion.id = tramo.id;
    tramoEvaluacion.initial_datetime = this.customISOstring(new Date());

    evaluacion.stretchs.push(tramoEvaluacion);
  }

  finalizaEvaluacionTramo(tramo: Tramo, evaluacion: Evaluacion, valoracionGeneral: any,
    congestion: number, anomalias: boolean) {

    var tramoEvaluacion;
    var indiceTramo;

    if(congestion == null || congestion == undefined){
      congestion = 0;
    }
    if(valoracionGeneral == null || valoracionGeneral == undefined){
      valoracionGeneral = 1;
    }
    if(anomalias == null || anomalias == undefined){
      anomalias = false;
    }

    tramoEvaluacion = evaluacion.stretchs.find(tram => tram.id === tramo.id);
    indiceTramo = evaluacion.stretchs.findIndex(tram => tram.id === tramo.id);
    console.info("Tramo: ", tramoEvaluacion, ", indice: ", indiceTramo);

    evaluacion.stretchs[indiceTramo].end_datetime = this.customISOstring(new Date());
    evaluacion.stretchs[indiceTramo].value = valoracionGeneral;
    evaluacion.stretchs[indiceTramo].anomalies = anomalias;
    evaluacion.stretchs[indiceTramo].congestion = congestion;

    evaluacion.stretchs[indiceTramo].gps = this.servicioSensores.ruta.slice();
    evaluacion.stretchs[indiceTramo].accelerometer = {
      x: this.servicioSensores.aceleracion.x.slice(),
      y: this.servicioSensores.aceleracion.y.slice(),
      z: this.servicioSensores.aceleracion.z.slice()
    };

    evaluacion.stretchs[indiceTramo].gyroscope = {
      x: this.servicioSensores.giroscopio.x.slice(),
      y: this.servicioSensores.giroscopio.y.slice(),
      z: this.servicioSensores.giroscopio.z.slice()
    };

    //Limpia arreglos
    if (this.servicioSensores.aceleracion.x) {
      this.servicioSensores.aceleracion.x.splice(0, this.servicioSensores.aceleracion.x.length);
      this.servicioSensores.aceleracion.y.splice(0, this.servicioSensores.aceleracion.y.length);
      this.servicioSensores.aceleracion.z.splice(0, this.servicioSensores.aceleracion.z.length);
    }

    if (this.servicioSensores.giroscopio.x) {
      this.servicioSensores.giroscopio.x.splice(0, this.servicioSensores.giroscopio.x.length);
      this.servicioSensores.giroscopio.y.splice(0, this.servicioSensores.giroscopio.y.length);
      this.servicioSensores.giroscopio.z.splice(0, this.servicioSensores.giroscopio.z.length);
    }

    this.servicioSensores.ruta.splice(0, this.servicioSensores.ruta.length);

    console.log("Evaluacion: ", evaluacion);
  }

  iniciaEvaluacionTransbordo(tramo: Tramo, evaluacion: Evaluacion) {
    var transbordoEvaluacion = new TransbordoEvaluacion();
    var transbordo = new Transbordo();

    transbordo.estacionDesde = tramo.inicio;
    transbordo.estacionHasta = tramo.fin;

    transbordoEvaluacion.initial_station = transbordo.estacionDesde.id;
    transbordoEvaluacion.end_station = transbordo.estacionHasta.id;
    transbordoEvaluacion.initial_datetime = this.customISOstring(new Date());

    this.evaluacion.transfers.push(transbordoEvaluacion);
  }

  finalizaEvaluacionTransbordo(tramo: Tramo, evaluacion: Evaluacion, congestion: number) {
    var transbordoEvaluacion;
    var indiceTransbordo;

    if(congestion == null || congestion == undefined){
      congestion = 0;
    }

    transbordoEvaluacion = evaluacion.transfers.find(trans => trans.initial_station === tramo.inicio.id);
    indiceTransbordo = evaluacion.transfers.findIndex(trans => trans.end_station === tramo.fin.id);

    evaluacion.transfers[indiceTransbordo].end_datetime = this.customISOstring(new Date());
    evaluacion.transfers[indiceTransbordo].congestion = congestion;
  }

  finalizaEvaluacionRuta(ruta: Ruta, evaluacion: Evaluacion) {

    var tramo = ruta.tramos[ruta.tramos.length - 1];

    if(this.congestion == null || this.congestion == undefined){
      this.congestion = 0;
    }

    evaluacion.end.station = tramo.fin.id;
    evaluacion.end.congestion = this.congestion;
    evaluacion.end.datetime = this.customISOstring(new Date());

    this.servicioSensores.detieneAcelerometro();
    this.servicioSensores.detieneGiroscopio();
    this.servicioSensores.detienePosicion();

    this.timer.unsubscribe(this.timerId);

    this.presentToast("Evaluación enviada al servidor, gracias por contribuir a la comunidad!","middle")

  }

  agregaEvaluacionTransbordo(tramo: Tramo, evaluacion: Evaluacion, congestion: number, horaFin: any) {
    var transbordoEvaluacion;
    var indiceTransbordo;
    var transbordo = new Transbordo();

    if(congestion == null || congestion == undefined){
      congestion = 0;
    }

    transbordo.estacionDesde = tramo.inicio;
    transbordo.estacionHasta = tramo.fin;

    if (evaluacion.transfers) {
      transbordoEvaluacion = evaluacion.transfers.find(trans => trans.initial_station === transbordo.estacionDesde.id);
      indiceTransbordo = evaluacion.transfers.findIndex(trans => trans.end_station === transbordo.estacionHasta.id);
      console.log("transbordo: ", transbordoEvaluacion, ", indice: ", indiceTransbordo);
    }

    if (!transbordoEvaluacion) {
      transbordoEvaluacion = new TransbordoEvaluacion();
      transbordoEvaluacion.initial_station = transbordo.estacionDesde.id;
      transbordoEvaluacion.end_station = transbordo.estacionHasta.id;
      transbordoEvaluacion.end_datetime = horaFin;
      transbordoEvaluacion.congestion = congestion;

      evaluacion.transfers.push(transbordoEvaluacion);
    } else {
      evaluacion.transfers[indiceTransbordo].end_datetime = horaFin;
      evaluacion.transfers[indiceTransbordo].congestion = congestion;
    }

    console.log("Evaluacion actualizada con transbordo: ", evaluacion);

    //Limpia arreglos
    this.servicioSensores.aceleracion.x.splice(0, this.servicioSensores.aceleracion.x.length);
    this.servicioSensores.aceleracion.y.splice(0, this.servicioSensores.aceleracion.y.length);
    this.servicioSensores.aceleracion.z.splice(0, this.servicioSensores.aceleracion.z.length);

    this.servicioSensores.giroscopio.x.splice(0, this.servicioSensores.giroscopio.x.length);
    this.servicioSensores.giroscopio.y.splice(0, this.servicioSensores.giroscopio.y.length);
    this.servicioSensores.giroscopio.z.splice(0, this.servicioSensores.giroscopio.z.length);

    this.servicioSensores.ruta.splice(0, this.servicioSensores.ruta.length);
  }

  agregaEvaluacionTramo(tramo: Tramo, evaluacion: Evaluacion, valoracionGeneral: any,
    congestion: number, anomalias: boolean,
    horaInicio: any, horaFin: any) {

    var tramoEvaluacion;
    var indiceTramo;

    if(congestion == null || congestion == undefined){
      congestion = 0;
    }
    if(valoracionGeneral == null || valoracionGeneral == undefined){
      valoracionGeneral = 1;
    }
    if(anomalias == null || anomalias == undefined){
      anomalias = false;
    }

    if (evaluacion.stretchs) {
      tramoEvaluacion = evaluacion.stretchs.find(tram => tram.id === tramo.id);
      indiceTramo = evaluacion.stretchs.findIndex(tram => tram.id === tramo.id);
      console.log("Tramo: ", tramoEvaluacion, ", indice: ", indiceTramo);
    }

    if (!tramoEvaluacion) {
      console.log("Tramo no encontrado");

      tramoEvaluacion = new TramoEvaluacion();
      tramoEvaluacion.id = tramo.id;
      tramoEvaluacion.initial_datetime = horaInicio;
      tramoEvaluacion.end_datetime = horaFin;

      tramoEvaluacion.value = valoracionGeneral;
      tramoEvaluacion.anomalies = anomalias;
      tramoEvaluacion.congestion = congestion;

      tramoEvaluacion.accelerometer = {
        x: this.servicioSensores.aceleracion.x.slice(),
        y: this.servicioSensores.aceleracion.y.slice(),
        z: this.servicioSensores.aceleracion.z.slice()
      };
  
      tramoEvaluacion.gyroscope = {
        x: this.servicioSensores.giroscopio.x.slice(),
        y: this.servicioSensores.giroscopio.y.slice(),
        z: this.servicioSensores.giroscopio.z.slice()
      };

      tramoEvaluacion.gps = this.servicioSensores.ruta.slice();

      console.log("Tramo agregado: ", tramoEvaluacion);

      evaluacion.stretchs.push(tramoEvaluacion);
    } else {
      evaluacion.stretchs[indiceTramo].end_datetime = horaFin;

      evaluacion.stretchs[indiceTramo].value = valoracionGeneral;
      evaluacion.stretchs[indiceTramo].anomalies = anomalias;
      evaluacion.stretchs[indiceTramo].congestion = congestion;

      console.log("Tramo modificado: ", evaluacion.stretchs[indiceTramo]);

      evaluacion.stretchs[indiceTramo].gps = this.servicioSensores.ruta.slice();

      evaluacion.stretchs[indiceTramo].accelerometer = {
        x: this.servicioSensores.aceleracion.x.slice(),
        y: this.servicioSensores.aceleracion.y.slice(),
        z: this.servicioSensores.aceleracion.z.slice()
      };
  
      evaluacion.stretchs[indiceTramo].gyroscope = {
        x: this.servicioSensores.giroscopio.x.slice(),
        y: this.servicioSensores.giroscopio.y.slice(),
        z: this.servicioSensores.giroscopio.z.slice()
      };
    }

    //Limpia arreglos
    this.servicioSensores.aceleracion.x.splice(0, this.servicioSensores.aceleracion.x.length);
    this.servicioSensores.aceleracion.y.splice(0, this.servicioSensores.aceleracion.y.length);
    this.servicioSensores.aceleracion.z.splice(0, this.servicioSensores.aceleracion.z.length);

    this.servicioSensores.giroscopio.x.splice(0, this.servicioSensores.giroscopio.x.length);
    this.servicioSensores.giroscopio.y.splice(0, this.servicioSensores.giroscopio.y.length);
    this.servicioSensores.giroscopio.z.splice(0, this.servicioSensores.giroscopio.z.length);

    this.servicioSensores.ruta.splice(0, this.servicioSensores.ruta.length);

    console.log("Evaluacion: ", evaluacion);

  }

  presentToast(mensaje, posicion) {
    let toastNuevo = this.toast.create({
      message: mensaje,
      position: posicion,
      closeButtonText: "Ok",
      showCloseButton: true
    });

    toastNuevo.present();

    toastNuevo.onDidDismiss(() => {
      console.log("DE VUELTA");
      this.navCtrl.goToRoot({animate:true});
    });
  }

  customISOstring(date: Date) {
    var date = new Date(date);
    date.setHours(date.getHours() - 4); // apply custom timezone
    function pad(n) { return n < 10 ? '0' + n : n }
    return date.getUTCFullYear() + '-' // return custom format
      + pad(date.getUTCMonth() + 1) + '-'
      + pad(date.getUTCDate()) + 'T'
      + pad(date.getUTCHours()) + ':'
      + pad(date.getUTCMinutes()) + ':'
      + pad(date.getUTCSeconds());
  }

}
