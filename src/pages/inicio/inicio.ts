import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { ServicioAutenticacionProvider } from '../../providers/servicio-autenticacion/servicio-autenticacion';
import { ServicioSensoresProvider } from '../../providers/servicio-sensores/servicio-sensores';
import { ServicioDatosProvider } from '../../providers/servicio-datos/servicio-datos';
import { ServicioTwitterProvider } from '../../providers/servicio-twitter/servicio-twitter';

import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the InicioPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html'
})
export class InicioPage {

  tweets;

  constructor(private loadingCtrl: LoadingController,
    public navCtrl: NavController, public navParams: NavParams,
    public servicioAutenticacion: ServicioAutenticacionProvider,
    public servicioSensores: ServicioSensoresProvider,
    public servicioDatos: ServicioDatosProvider,
    public servicioTwitter: ServicioTwitterProvider,
    public browser: InAppBrowser) {

    console.log('-----------> PAGINA INICIO CARGADA');
    this.cargaFeedTwitter(null);
  }

  ionViewDidLoad() {
    try {
      this.cargarDatos();

    } catch (error) {

    }
    console.log('pagina cargada');
  }

  cargarDatos() {
    this.servicioDatos.getEstaciones()
      .then(() => {
        this.servicioDatos.getLineas()
          .then(() => {

          })
          .catch((excepcion) => {
            console.log(excepcion);
          });;
      })
      .catch((excepcion) => {
        console.log(excepcion);
      });
    this.servicioDatos.getTrayectos().then(() => {

    }).catch((excepcion) => {
        console.log(excepcion);
      });
  }

  cargaFeedTwitter(refresher) {
    this.servicioTwitter.sync().then(
      data => {
        this.tweets = data;
        if(refresher){
          refresher.complete();
        }
      }, err => {
        console.log("Error: ", err);
        if(refresher){
          refresher.complete();
        }
      }
    );
  }

  fechaParaTweet(fechaString) {
    let d = new Date(Date.parse(fechaString));

    // http://stackoverflow.com/questions/3552461/how-to-format-a-javascript-date
    var fecha = ("0" + d.getDate()).slice(-2) + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" +
      d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);

    return fecha;
  }

  abrirLink(url) {
    let browser = this.browser.create(url, 'blank');
    browser.show();
  }

}
