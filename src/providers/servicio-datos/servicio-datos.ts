﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeout';
import { Observable } from 'rxjs/Observable';

import { Linea, Estacion, Equipamiento, Evaluacion } from '../../models/modelos';
import { ServicioAutenticacionProvider } from '../servicio-autenticacion/servicio-autenticacion';

import { EnvVariables } from '../../app/variables-entorno/variables-entorno.token';

@Injectable()
export class ServicioDatosProvider {

  estaciones: Estacion[] = new Array();
  lineas: Linea[] = new Array();

  BASE_URL = '/api_rest/';

  constructor(public http: Http, public servicioAutenticacion: ServicioAutenticacionProvider, @Inject(EnvVariables) public envVariables) {
    this.BASE_URL = this.envVariables.apiEndpoint + this.BASE_URL;
  }

  ngOnInit() {
    this.loadData().then( ok => {
      console.log("datos de bd:", ok);
      if(ok){
        console.log("Datos cargados desde la bd");
      }
    }).catch(this.catchError);
  }

  //storeLines(lineas: any) {
  //    return this.servicioAutenticacion.storage.set('lines', lineas);
  //}

  storeStationLines(estaciones: any) {
      return this.servicioAutenticacion.storage.set('stationlines', estaciones);
  }

  /*loadData() {
    return this.servicioAutenticacion.storage.get('stationlines').then( data => {
        console.log("StationLines: ", data);
        this.estaciones = data;
      }).then(() => {
        this.servicioAutenticacion.storage.get('lines').then( data => {
          console.log("Lineas: ", data);
          this.lineas = data;
        });
      });
  }*/
  loadData() {
    return this.servicioAutenticacion.storage.get('stationlines').then(this.extraeDatos);
  }

  private post(url, parametros, contentType) {
    var headers = new Headers();
    headers.append('Content-Type', contentType);
    headers.append('Authorization', 'Token ' + this.servicioAutenticacion.AuthToken);

    return new Promise(resolve => {
        this.http.post(url, parametros, {headers: headers})
          .timeout(this.envVariables.timeout)
          .map(res => res.json())
          .catch(this.catchError)
        .subscribe(data => {

          if(resolve(data)){
            resolve(data);
          }else{
            resolve(false);
          }

        });
    });
  }

  private get(url){
    return new Promise(resolve => {
      var headers = new Headers();
      this.servicioAutenticacion.loadUserCredentials();

      headers.append('Authorization', 'Token ' + this.servicioAutenticacion.AuthToken);

      this.http.get(url, {headers: headers})
        .timeout(this.envVariables.timeout)
        .map(res => res.json())
        .catch(this.catchError)
      .subscribe(data => {
          if(data) {
            resolve(data);
          } else {
            resolve(false);
          }
      })
    });

  }


  getLineas(){
    
    return new Promise(resolve => {
      if(this.lineas.length > 0){
          resolve(this.lineas);
      }else{
        resolve(false);
      }
    });
    /*else{
      let url = BASE_URL + 'lines/?format=json';
      return this.get(url).then( data => {
        this.storeStationLines(data);
        return data;
      });
    }*/
    
  }
  


  getEstaciones(){
    
    if(this.estaciones.length > 0){
      return new Promise(resolve => {
        resolve(this.estaciones);
      });
    }else{
      let url = this.BASE_URL + 'stations/?format=json';
      return this.get(url).then( data => {
        console.log("DATOS: ", data);
        
        return new Promise(resolve => {
          this.storeStationLines(data).then(() => {
            this.extraeDatos(data);

            resolve(this.estaciones);
          });
        
        });
      });
    }
  }
  

  getLinea(idLinea: number){
    
    if(this.lineas.length > 0){
      return new Promise(resolve => {
        resolve(
          this.lineas.find(linea => {
            return linea.id == idLinea;
          })
        )
      });
    }else{
      /*let url = BASE_URL + 'lines/' + idLinea + '/?format=json';
      return this.get(url);*/
      return this.getEstaciones().then((data) => {
          return this.getLinea(idLinea);
      });
    }

  }

  getEstacion(idEstacion: number){
    
    if(this.estaciones.length > 0){
      return new Promise(resolve => {
        var estacionSalida = this.estaciones.find(estacion => {
          return estacion.id == idEstacion;
        });
        resolve(estacionSalida);
      });
    }else{
      return this.getEstaciones().then((data) => {
          return this.getEstacion(idEstacion);
      });
    }
    
  }

/*
  getEstacion(idEstacion: number){
    if(this.estaciones){
      return new Promise(resolve => {
        this.lineas.forEach(linea => {
          var estacion = linea.estaciones.find(estacion => {
            return estacion.id == idEstacion;
          });
          resolve(estacion);
        });
      });
    }else{
      let url = BASE_URL + 'stations/' + idEstacion + '/?format=json';
      return this.get(url);
    }
    
  }
  */

  getTrayectos(){
    let url = this.BASE_URL + 'travels/?format=json';
    return this.get(url);
  }

  guardarTrayecto(estacionInicio: number, estacionFin: number){
    let url = this.BASE_URL + 'travels/';
    let parametros = "initial_station=" + estacionInicio + "&end_station=" + estacionFin + "&user=" + this.servicioAutenticacion.user.iduser;
    return this.post(url, parametros, 'application/x-www-form-urlencoded');
  }

  enviaEvaluacion(evaluacion: Evaluacion){
    let url = this.BASE_URL + 'results/';
    let parametros =  JSON.stringify(evaluacion);
    return this.post(url, parametros, "application/json");
  }

  private catchError(error: Response | any){
    console.log(error);
    return Observable.throw(error || "Server error.");
  }

  extraeDatos(data: any) {
      
      //console.log("Extrayendo datos: ", data);

      data.forEach((linea: any) => {

        var idLinea = 0;
        var newLinea = new Linea();
        newLinea.name = linea.name;
        if(linea.color){
          newLinea.color = "#"+linea.color;
        }

        linea.stations.forEach((estacion) => {
          var newEstacion = new Estacion();
          newEstacion.id = estacion.id;
          newEstacion.idLinea = estacion.line;

          idLinea = newEstacion.idLinea;

          newEstacion.name = estacion.station.name;
          newEstacion.accesibilidad = estacion.accessibility;
          
          estacion.equipment.forEach((equipamiento) =>{
            var newEquipamiento = new Equipamiento();
            newEquipamiento.name = equipamiento.name;
            newEstacion.equipamiento.push(newEquipamiento);
          });

          if(estacion.express_service){
            newEstacion.colorServicioExpress = "#"+estacion.express_service.color;
          }

          newLinea.estaciones.push(newEstacion);
          this.estaciones.push(newEstacion);
        });
        
        newLinea.id = idLinea;
        this.lineas.push(newLinea);
        
      });

      //console.log("Lineas leidas: ", this.lineas);
    }

}
