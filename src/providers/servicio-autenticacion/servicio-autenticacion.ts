﻿import {
  Injectable, Inject
} from '@angular/core';
import {
  Http,
  Headers
} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeout';
import {
  Observable
} from 'rxjs/Observable';
import {
  Storage
} from '@ionic/storage';

import { EnvVariables } from '../../app/variables-entorno/variables-entorno.token';

export class User {
  iduser: string = "";
  username: string = "";
  email: string = "";

  constructor(username: string, email: string) {
    this.username = username;
    this.email = email;
  }
}

@Injectable()
export class ServicioAutenticacionProvider {

  user: User;
  isLoggedin: boolean;
  AuthToken: string;
  

  constructor(public http: Http, public storage: Storage, @Inject(EnvVariables) public envVariables) {
    this.isLoggedin = false;
  }

  storeUserCredentials(token: string) {
    return this.storage.set('token', token).then(() => {
      this.useCredentials(token);
    });
  }

  storeUserID(idusuario: string) {
    return this.storage.set('idusuario', idusuario);
  }

  useCredentials(token: string) {
    this.isLoggedin = true;
    this.AuthToken = token;
  }

  loadUserCredentials() {
    return this.storage.get('token').then(data => {
      if (data) {
        this.useCredentials(data);
      }
    });
  }

  loadUserID() {
    return this.storage.get('idusuario').then(data => {
      if (data) {
        this.user.iduser = data;
      }
    });
  }

  private get(url, headers, timeout) {

    return new Promise((resolve, reject) => {
      this.http.get(url, {
          headers: headers
        })
        .timeout(timeout)
        .map(res => res.json())
        .catch((error: Response) => {
          reject(error);
          return Observable.throw(error);
        })
        .subscribe(data => {
          if (data) {
            resolve(data);
          } else {
            reject(false);
          }
        }, error => {
          reject(error);
        })


    });

  }

  private post(url, creds, headers, timeout) {

    return new Promise((resolve, reject) => {
      this.http.post(url, creds, {
          headers: headers
        })
        .timeout(timeout)
        .map(res => res.json())
        .catch((error: Response) => {
          reject(error);
          return Observable.throw(error);
        })
        .subscribe(data => {
          resolve(data);
        }, error => {
          reject(error);
        });
    });


  }

  destroyUserCredentials() {
    this.isLoggedin = false;
    this.AuthToken = null;
    this.storage.remove('token');
    this.storage.remove('idusuario');
  }

  // authenticate(username: string, password: string) {
  //   var creds = "username=" + username + "&password=" + password;
  //   var headers = new Headers();
  //   headers.append('Content-Type', 'application/x-www-form-urlencoded');
  //   return new Promise((resolve, reject) => {
  //       this.http.post(BASE_URL + 'api-token-auth/login/', creds, {headers: headers})
  //       .timeout(3000)
  //       .catch((error: Response) => {
  //           reject(error);
  //           return Observable.throw(error);
  //         })
  //       .subscribe(data => {
  //           if(data.json().token) {
  //               this.storeUserCredentials(data.json().token).then(() => {
  //                   this.getMe().then(() => {
  //                     resolve(true);
  //                   });
  //               });
  //           } else {
  //               resolve(false);
  //           }

  //       }, error => {
  //         reject(error);
  //       });
  //   });
  // }

  authenticate(username: string, password: string) {
    var url = this.envVariables.apiEndpoint + '/api-token-auth/login/'
    var creds = "username=" + username + "&password=" + password;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return new Promise((resolve, reject) => {
      this.post(url, creds, headers, this.envVariables.timeout)
        .then(
          (data: any) => {
            if (data.token) {
              this.storeUserCredentials(data.token)
                .then(() => {
                    this.getMe().then(
                        obtenido => {
                          resolve(true);
                        },
                        error => {
                          reject(error);
                        });
                  }
                );
            }
          },
          error => {
            reject(error);
          });
    });
  }

  getMe() {
    var url = this.envVariables.apiEndpoint + '/auth/me/';
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Token ' + this.AuthToken);

    return new Promise((resolve, reject) => {
      this.get(url, headers, this.envVariables.timeout)
        .then(
          (data: any) => {
            if (data.username && data.email) {
              this.user = new User(data.username, data.email);

              this.loadUserID()
              .then(() => {
                  if (!this.user.iduser) {
                    console.log("id de usuario no encontrado, almacenando en bd " + data.id);
                    this.storeUserID(data.id);
                  }
                  resolve(true);
                }
              );

            } else {
              reject(data);
            }
          }, error => {
            reject(error);
          }
        )
    });
  }

  /*getMe() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Token ' + this.AuthToken);

    return new Promise((resolve, reject) => {
      this.http.get(BASE_URL + 'auth/me/', {headers: headers})
        .timeout(3000)
        .map(res => res.json())
        .catch((error: Response) => {
          reject(error);
          return Observable.throw(error);
        })
      .subscribe(data => {
          if(data) {
            this.user = new User(data.username, data.email);

            this.loadUserID().then(() => {
              if(!this.user.iduser){
                console.log("id de usuario no encontrado, almacenando en bd");
                this.storeUserID(data.id);
              }
            }).then(() => {
              resolve(true);
            });

          } else {
            resolve(false);
          }
      }, error => {
          reject(error);
      })
    });
  }*/

  registro(username: string, password: string, email: string) {
    var creds = "username=" + username + "&password=" + password + "&email=" + email;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return new Promise((resolve, reject) => {
      this.http.post(this.envVariables.apiEndpoint + '/auth/register/', creds, {
          headers: headers
        })
        .timeout(this.envVariables.timeout)
        .map(res => res.json())
        .catch((error: Response) => {
          reject(error);
          return Observable.throw(error);
        })
        .subscribe(data => {

          if (data) {
            resolve(true);
          } else {
            reject(false);
          }
        }, error => {
          reject(error);
        });
    });
  }

  /*getinfo() {
      return new Promise(resolve => {
          var headers = new Headers();
          this.loadUserCredentials();
          console.log(this.AuthToken);
          headers.append('Authorization', 'Token ' +this.AuthToken);
          this.http.get(BASE_URL + 'getinfo/', {headers: headers}).subscribe(data => {
              if(data.json().success) {
                resolve(data.json());
              } else {
                resolve(false);
              }
          });
      })
  }*/

  logout() {
    this.destroyUserCredentials();
  }


}
