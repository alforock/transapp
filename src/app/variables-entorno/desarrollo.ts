export const devVariables = {
    apiEndpoint: 'https://transapp-server.herokuapp.com',
    environmentName: 'Entorno de Desarrollo',
    ionicEnvName: 'dev',
    timeout: 30000,
    accfreq: 3000,
    girofreq: 3000
  };