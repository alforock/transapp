import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeout';
import { Observable } from 'rxjs/Observable';

import { ServicioAutenticacionProvider } from '../servicio-autenticacion/servicio-autenticacion';

import { EnvVariables } from '../../app/variables-entorno/variables-entorno.token';

@Injectable()
export class ServicioPlanificacionProvider {

  BASE_URL = '/api_rest/';

  horariosExpress = {
      am: {inicio:600,fin:900},
      pm: {inicio:1800,fin:2100}
  };

  constructor(private http: Http, public servicioAutenticacion: ServicioAutenticacionProvider, @Inject(EnvVariables) public envVariables) {
    console.log('Hello ServicioPlanificacionProvider Provider');
    this.BASE_URL = this.envVariables.apiEndpoint + this.BASE_URL;
  }

  private get(url){

    return new Promise( (resolve, reject) => {
      var headers = new Headers();
      this.servicioAutenticacion.loadUserCredentials();

      headers.append('Authorization', 'Token ' + this.servicioAutenticacion.AuthToken);
      this.http.get(url, {headers: headers})
        .timeout(this.envVariables.timeout)
        .map(res => res.json())
        .catch((error: Response) => {
              reject(error);
              return Observable.throw(error);
            })
      .subscribe(data => {
          if(data) {
            resolve(data);
          } else {
            resolve(false);
          }
      }, error => {
        reject(error);
      })


    });


  }

  getNearStation(posX: number, posY: number){
    let url = this.BASE_URL + 'near_station/?format=json&lat=' + posX + '&lon=' + posY;
    return this.get(url);
  }

  getPath(estacionIni: number, estacionFin: number){
    let url = this.BASE_URL + 'paths/?format=json&start=' + estacionIni + '&end=' + estacionFin;
    return this.get(url);
  }

  getExpressPath(estacionIni: number, estacionFin: number){
    let url = this.BASE_URL + 'express_paths/?format=json&start=' + estacionIni + '&end=' + estacionFin;
    return this.get(url);
  }

}
