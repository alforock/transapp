import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { DeviceMotion } from '@ionic-native/device-motion';
import { DBMeter } from '@ionic-native/db-meter';
import { Diagnostic } from '@ionic-native/diagnostic';
import { BackgroundMode } from '@ionic-native/background-mode';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Gyroscope } from '@ionic-native/gyroscope';
import { AppVersion } from '@ionic-native/app-version';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { RegistroPage } from '../pages/registro/registro';
import { InicioPage } from '../pages/inicio/inicio';
import { EvaluacionPage } from '../pages/evaluacion/evaluacion';
import { PlanificacionPage } from '../pages/planificacion/planificacion';
import { TrayectosPage } from '../pages/trayectos/trayectos';
import { EstacionPage } from '../pages/estacion/estacion';
import { ServicioPlanificacionProvider } from '../providers/servicio-planificacion/servicio-planificacion';
import { ServicioAutenticacionProvider } from '../providers/servicio-autenticacion/servicio-autenticacion';
import { ServicioDatosProvider } from '../providers/servicio-datos/servicio-datos';
import { ServicioSensoresProvider } from '../providers/servicio-sensores/servicio-sensores';
import { FiltroatributoPipe } from '../pipes/filtroatributo/filtroatributo';

import { SimpleTimer } from 'ng2-simple-timer';

import { EnvironmentsModule } from './variables-entorno/variables-entorno.module';
import { ServicioTwitterProvider } from '../providers/servicio-twitter/servicio-twitter';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    RegistroPage,
    InicioPage,
    EvaluacionPage,
    PlanificacionPage,
    TrayectosPage,
    EstacionPage,
    FiltroatributoPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    EnvironmentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    RegistroPage,
    InicioPage,
    EvaluacionPage,
    PlanificacionPage,
    TrayectosPage,
    EstacionPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DeviceMotion,
    DBMeter,
    Diagnostic,
    BackgroundGeolocation,
    Geolocation,
    BackgroundMode,
    AppVersion,
    Gyroscope,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServicioPlanificacionProvider,
    ServicioAutenticacionProvider,
    ServicioDatosProvider,
    ServicioSensoresProvider,
    SimpleTimer,
    ServicioTwitterProvider,
    InAppBrowser
  ]
})
export class AppModule {}
