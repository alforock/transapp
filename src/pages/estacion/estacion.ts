import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';

import { ServicioPlanificacionProvider } from '../../providers/servicio-planificacion/servicio-planificacion';
import { ServicioDatosProvider } from '../../providers/servicio-datos/servicio-datos';
import { ServicioSensoresProvider } from '../../providers/servicio-sensores/servicio-sensores';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Estacion, Linea } from '../../models/modelos';

/**
 * Generated class for the EstacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-estacion',
  templateUrl: 'estacion.html',
})
export class EstacionPage {

  estacion: Estacion;
  linea: Linea;
  estaciones: Array<Estacion>;
  distanciaKm: string;

  muestraEquip: boolean = false;

  form: FormGroup;

  color;

  constructor(public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public toast: ToastController,
    public servicioDatos: ServicioDatosProvider,
    public servicioPlanificacion: ServicioPlanificacionProvider,
    public servicioSensores: ServicioSensoresProvider) {

    this.form = this.formBuilder.group({
      estacion: ['', Validators.required],
      linea: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    this.obtenerDatos();
  }

  obtenerDatos() {
    let loading = this.loadingCtrl.create({
      content: 'Buscando la estación más cercana, espere...'
    });
    loading.present();

    this.servicioSensores.getPosicionActual()
    .then((ubicacion) => {
      console.log(ubicacion);

      if (ubicacion) {
        this.servicioPlanificacion.getNearStation(ubicacion.coords.latitude, ubicacion.coords.longitude).then((estacionMasCercana: any) => {
          
          this.servicioDatos.getLinea(estacionMasCercana.station_line.line).then(linea => {
            this.linea = linea;
            
            this.servicioDatos.getEstacion(estacionMasCercana.station_line.id).then(estacion => {
              this.estacion = estacion;
              var latLon = this.obtieneCoordenadas(estacionMasCercana.coordinates);
              if(ubicacion.coords.latitude != 0 && ubicacion.coords.longitude != 0 && latLon.lat != 0 && latLon.lon != 0) {
                this.distanciaKm = this.calcularDistanciaKm(ubicacion.coords.latitude, ubicacion.coords.longitude, latLon.lat, latLon.lon);
              }

              loading.dismiss();
            });
          });
        }, e => {
          loading.dismiss();
          this.cargaDatos();
          this.presentToast("Error al obtener la estación más cercana, por favor ingrese su estación más cercana de forma manual", "bottom");
          console.log("Error al obtener la estacion más cercana: ", e);
        }).catch(this.loguearExcepcion);
      } else {
        loading.dismiss();
        this.cargaDatos();
        this.presentToast("El servicio de ubicación no se está ejecutando, por favor ingrese su estación más cercana de forma manual", "bottom");
      }

    }, e => {
      loading.dismiss();
      this.cargaDatos();
      console.log("Error al obtener la ubicación actual: ", e);
    })
    .catch(this.loguearExcepcion);

  }

  cargaDatos() {
    let loading = this.loadingCtrl.create({
      content: 'Buscando estaciones, espere...'
    });
    loading.present();

    this.servicioDatos.getEstaciones()
      .then(() => {
        this.servicioDatos.getLineas()
          .then(data => {
            console.log("Datos de lineas obtenido", data);
            this.presentToast("Datos de líneas y estaciones obtenidos", "bottom");
            loading.dismiss();
          }, (err) => {
            console.log(err);
          });
      }).catch((excepcion) => {
        console.log(excepcion);
      });

  }

  enviarForm() {

    let loading = this.loadingCtrl.create({
      content: 'Buscando estación...'
    });
    loading.present();

    this.servicioDatos.getLinea(this.form.value.linea.id).then(linea => {
      this.linea = linea;

      this.servicioDatos.getEstacion(this.form.value.estacion.id).then(estacion => {
        this.estacion = estacion;

        loading.dismiss();
      });
    });

  }

  setValoresEstaciones(linea: Linea) {
    this.estaciones = linea.estaciones;
  }

  calcularDistanciaKm(lat1, lon1, lat2, lon2) {
    var R = 6378.137; //Radio de la tierra en km
    var dLat = this.rad(lat2 - lat1);
    var dLong = this.rad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.rad(lat1)) * Math.cos(this.rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;

    console.log("Distancia calculada: ", d);

    return d.toFixed(3); //Retorna tres decimales
  }

  obtieneCoordenadas(coordenadas: string) {
    var coords = coordenadas.substring(coordenadas.indexOf("(") + 1, coordenadas.lastIndexOf(")"));

    var x: number;
    var y: number;
    
    try {
      y = parseFloat(coords.split(" ")[1].trim());
      x = parseFloat(coords.split(" ")[0].trim());
    } catch (error) {
      console.log(error);
      x = y = 0;
    }

    return {"lat": x, "lon": y};
  }

  rad(x) {
    return x * Math.PI / 180;
  }

  loguearExcepcion(excep) {
    console.log("Error: ", excep);
  }

  muestraEquipamiento(){
    this.muestraEquip = this.muestraEquip?false:true;
  }

  presentToast(mensaje, posicion) {
    let toastNuevo = this.toast.create({
      message: mensaje,
      duration: 2000,
      position: posicion
    });

    toastNuevo.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toastNuevo.present();
  }

}
