import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { InicioPage } from '../pages/inicio/inicio';
import { EstacionPage } from '../pages/estacion/estacion';
import { PlanificacionPage } from '../pages/planificacion/planificacion';
import { TrayectosPage } from '../pages/trayectos/trayectos';

import { ServicioAutenticacionProvider } from '../providers/servicio-autenticacion/servicio-autenticacion';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('NAV') nav: Nav;
  rootPage: any;
  pages: Array<{ titulo: string, componente: any, icon: string }>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private alertCtrl: AlertController,
    public servicioAutenticacion: ServicioAutenticacionProvider) {

    this.rootPage = LoginPage;
    this.pages = [
      { titulo: 'Inicio', componente: InicioPage, icon: 'home' },
      { titulo: 'Mi estación', componente: EstacionPage, icon: 'pin' },
      { titulo: 'Planificar Viaje', componente: PlanificacionPage, icon: 'navigate' },
      { titulo: 'Trayectos', componente: TrayectosPage, icon: 'analytics' }
    ];

    platform.ready().then(() => {
        statusBar.styleDefault();
        splashScreen.hide();
    });
  }

  cerrarSesion() {
    let alert = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Está seguro que desea salir?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.servicioAutenticacion.destroyUserCredentials();
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }

  goToPage(page){
    this.nav.setRoot(page);
  }
}
