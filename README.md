Proyecto generado con Ionic 3 y Cordova.

Para comenzar con el proyecto se debe ejecutar el siguiente comando:

```bash
$ ionic cordova prepare
$ ionic serve
```

Luego para ejecutar:

```bash
$ cordova build android
$ cordova emulate android
```
