import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrayectosPage } from './trayectos';

@NgModule({
  declarations: [
    TrayectosPage,
  ],
  imports: [
    IonicPageModule.forChild(TrayectosPage),
  ],
  exports: [
    TrayectosPage
  ]
})
export class TrayectosPageModule {}
