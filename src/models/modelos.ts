export class Equipamiento {
    name: string;
}

export class Estacion {
    id: number;
    name: string;
    esTerminal: boolean;
    colorServicioExpress: string;
    idLinea: number;
    equipamiento: Equipamiento[] = new Array();
    accesibilidad: boolean;
    coordenadas = {
        x: 0,
        y: 0
    };
}

export class Linea {
    id: number;
    name: string;
    estaciones: Estacion[] = new Array();
    color: string;
}

export class Trayecto {
    lineaInicio: Linea;
    estacionInicio: Estacion;
    lineaFinal: Linea;
    estacionFinal: Estacion;
}

export class Tramo {
    id: number;
    inicio: Estacion;
    fin: Estacion;
}

export class Transbordo {
    estacionDesde: Estacion;
    estacionHasta: Estacion;
    lineaDesde: Linea;
    lineaHasta: Linea;
}

export class Ruta {
    transbordos: Transbordo[] = new Array();
    tramos: Tramo[] = new Array();
}

export class TransbordoEvaluacion {
    initial_station: number;
    end_station: number;
    congestion: number;
    initial_datetime: string;
    end_datetime: string;
}

export class Gps {
    lat: number;
    lon: number;
    datetime: string;
}

export class TramoEvaluacion {
    id: number;
    initial_datetime: string;
    end_datetime: string;
    value: number;
    congestion: number;
    anomalies: boolean;
    accelerometer: {
        x: number[],
        y: number[],
        z: number[]
    };
    gyroscope: {
        x: number[],
        y: number[],
        z: number[]
    };
    gps: Gps[] = new Array();
}

export class Evaluacion {
    start = {
        station: 0,
        congestion: 0,
        datetime: ""
    };
    end = {
        station: 0,
        congestion: 0,
        datetime: ""
    };
    transfers: TransbordoEvaluacion[] = new Array();
    is_express: boolean;
    accelerometer_frequency: number;
    gyroscope_frequency: number;
    stretchs: TramoEvaluacion[] = new Array();
}