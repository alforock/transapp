import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';

import { ServicioAutenticacionProvider } from '../../providers/servicio-autenticacion/servicio-autenticacion';
import { InicioPage } from '../../pages/inicio/inicio';
import { RegistroPage } from '../../pages/registro/registro';

//import { FiltroatributoPipe } from '../../pipes/filtroatributo/filtroatributo';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
/*@Pipe({
  name: 'dateFormatPipe',
})*/
export class LoginPage {

  loading: Loading;
  usuario: string;
  password: string;
  //email: string;
  createSuccess = false;

  version: any;

  constructor(private appVersion: AppVersion,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public servicioAutenticacion: ServicioAutenticacionProvider) {

    this.appVersion.getVersionNumber().then(version => {
      this.version = version;
    }, e => {
      console.log(e);
    });

  }

  ionViewDidLoad() {
    this.servicioAutenticacion.loadUserCredentials().then(() => {

      if(this.servicioAutenticacion.AuthToken){

        console.log(this.servicioAutenticacion.AuthToken);
        this.servicioAutenticacion.getMe().then(data => {
          console.log(data);
            if(data){
              this.navCtrl.setRoot(InicioPage);
            };
        },
        error => {
          console.log(error);
        });

      }
    });
  }

  public login() {
      this.showLoading();

      this.servicioAutenticacion.authenticate(this.usuario, this.password)
      .then(
        allowed => {
          console.log(allowed);
          if (allowed) {
            this.navCtrl.setRoot(InicioPage);
          } else {
            this.showError("Usuario o contraseña incorrecta");
          }
        },
        error => {
          console.log("ERROR: ", error);


          try {
            if(error.json()){
              error = error.json();
              var keys = Object.keys(error);
              this.showError(error[keys[0]]);
            }else{
              this.showError(error);
            }
          } catch (e) {
            console.log("Error:", e);

            if(error.name && error.name === 'TimeoutError'){
              this.showError('El servidor no responde');
            }else{
              this.showError(error);
            }
          }

        }
      );
  }

  public registrarse(){
    this.navCtrl.push(RegistroPage);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Espere un momento...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.navCtrl.popToRoot();
            }
          }
        }
      ]
    });
    alert.present();
  }

}
