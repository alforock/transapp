import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Base64 } from './Base64';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeout';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the ServicioTwitterProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ServicioTwitterProvider {

  authorization = null;
  consumerKey = "uh9pOSmbEKfb1TVgHYut47iYp";
  consumerSecret = "xY1arOFgrwaC4v6OpCUeBidHvbI3Q0afmqqHlgUsQAI4ChqmTC";
  twitterTokenURL = "https://api.twitter.com/oauth2/token";
  twitterStreamURL = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name="; //url query, this one is for hash tags
  qValue = "@metrodesantiago"; //hash tag %23 is for #
  numberOfTweets = "&count=10";
  excluyeResp = "&exclude_replies=true"

  constructor(public http: Http) {
    console.log('Hello ServicioTwitterProvider Provider');

  }

  sync() {
    /*headers.append("Access-Control-Allow-Origin", "*");
    headers.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");*/

    var url = this.twitterStreamURL + this.qValue + this.numberOfTweets + this.excluyeResp;

    return new Promise(resolve => {
      //get authorization token
      this.getAuthorization().then(() => {

        var headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authorization.access_token);
        headers.append('Content-Type', 'application/json');
        headers.append('cache', 'true');

        // make request with the token
        this.http.get(url, {headers: headers})
          .map(res => res.json())
          .catch(this.catchError)
          .subscribe( data => {
            console.log("Datos: ", data);

            if(data){
              resolve(data);
            }else{
              resolve(false);
            }
          })
      });
    });
  }

  getAuthorization() {
    var combined = encodeURIComponent(this.consumerKey) + ":" + encodeURIComponent(this.consumerSecret);
    var base64Encoded = new Base64().encode(combined);

    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
    headers.append('Authorization', 'Basic ' + base64Encoded);
    /*headers.append("Access-Control-Allow-Origin", "*");
    headers.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");*/

    var parametros = "grant_type=client_credentials";

    return new Promise(resolve => {
      /*if(this.authorization){
        resolve(this.authorization);
      }*/

      // Get the token
      this.http.post(this.twitterTokenURL, parametros, {headers: headers})
        .map(res => res.json())
        .catch(this.catchError)
        .subscribe( data => {
          console.log("Auth: ", data);
          
          if (data && data.token_type && data.token_type === "bearer") {
            this.authorization = data;

            resolve(data);
          } else {
            console.log("Error: ", data);
            resolve(false);
          }
        });
    });

  }

  private catchError(error: Response | any){
    console.log("ERROR -> ", error);
    return Observable.throw(error || "Server error.");
  }

}
