import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { Gps } from '../../models/modelos';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion';
import { Gyroscope, GyroscopeOrientation } from '@ionic-native/gyroscope';
import { DBMeter } from '@ionic-native/db-meter';
import { Diagnostic } from '@ionic-native/diagnostic';
import { BackgroundMode } from '@ionic-native/background-mode';

import { EnvVariables } from '../../app/variables-entorno/variables-entorno.token';


export class Aceleracion {
	x: number[] = [];
	y: number[] = [];
	z: number[] = [];

	Aceleracion() {
	}
}

export class Giroscopio {
	x: number[] = [];
	y: number[] = [];
	z: number[] = [];

	Giroscopio() {
	}
}

export class Activado {
	bluetooth: boolean;
	giroscopio: boolean;
	microfono: boolean;
	gps: boolean;
	movimiento: boolean;
}

export class Permiso {
	bluetooth: boolean;
	giroscopio: boolean;
	microfono: boolean;
	gps: boolean;
}

@Injectable()
export class ServicioSensoresProvider {

	aceleracion: Aceleracion = new Aceleracion();
	giroscopio: Giroscopio = new Giroscopio();
	ruta: Gps[] = [];
	activado: Activado = new Activado();
	permiso: Permiso = new Permiso();

	watchAceleracion: any;
	watchGiroscopio: any;
	watchGPS: any;

	ruido: any;

	opciones: DeviceMotionAccelerometerOptions;

	/*config: BackgroundGeolocationConfig = {
		desiredAccuracy: 10,
		stationaryRadius: 20,
		distanceFilter: 30,
		debug: true, //  enable this hear sounds for background-geolocation life-cycle.
		stopOnTerminate: false, // enable this to clear background location settings when the app terminates
	};*/


	constructor(//public backgroundGeolocation: BackgroundGeolocation,
		private gps: Geolocation, public diagnostico: Diagnostic,
		public ruidoAmbiental: DBMeter, public acelerometro: DeviceMotion,
		public platform: Platform, public http: Http,
		public background: BackgroundMode, public giroscopioSensor: Gyroscope,
		@Inject(EnvVariables) public envVariables) {
		console.log('Hello ServicioSensoresProvider Provider');
		console.log("---------- DIAGNOSTICO INI: \n", this.diagnostico, "\n---------- DIAGNOSTICO FIN");

		/*this.background.excludeFromTaskList();
		this.background.enable();*/

		/*this.backgroundGeolocation.configure(this.config).subscribe((location: BackgroundGeolocationResponse) => {
			var gpsNuevo = new Gps();
			gpsNuevo.lat = location.latitude;
			gpsNuevo.lon = location.longitude;

			this.ruta.push(gpsNuevo);
			console.log("Ubicacion: ", location);
		});*/

	}


	ngOnInit() {
		if (this.platform.ready()) {
			console.log("PLATAFORMA LISTA!!!");

			this.diagnostico.getLocationAuthorizationStatus()
				.then((state) => {
					console.log("Estado de atorizacion de gps: ", state);
					this.permiso.microfono = state;
				}, e => {
					console.log(e);
				}).catch(e => console.log(e));

			this.diagnostico.isMicrophoneAuthorized()
				.then((state) => {
					console.log("Permiso para escuchar microfono: ", state);
					this.permiso.microfono = state;
				}, e => {
					console.log(e);
				}).catch(e => console.log(e));

			this.diagnostico.isGpsLocationEnabled()
				.then((state) => {
					console.log("Permiso para leer gps: ", state);
					this.activado.gps = state;
				}, e => {
					console.log(e);
				}).catch(e => console.log(e));

			this.diagnostico.isMotionAvailable()
				.then((state) => {
					console.log("Permiso para leer movimiento: ", state);
					this.activado.movimiento = state;
				}, e => {
					console.log(e);
				}).catch(e => console.error(e));

			this.ruidoAmbiental.isListening().then(
				(isListening: boolean) => {
					if (!isListening) {
						this.ruido = this.ruidoAmbiental.start().subscribe(
							data => console.log("Ruido ambiental: ", data)
						);
					}
				}, e => {
					console.log(e);
				}).catch(e => console.error(e));
		}
	}

	iniciaAcelerometro() {
		console.info("Iniciando lectura de la aceleracion", this.watchAceleracion);
		var options = { frequency: this.envVariables.accfreq };
		if(!this.watchAceleracion){
			this.watchAceleracion = this.acelerometro.watchAcceleration(options).subscribe(
				(aceleracion: DeviceMotionAccelerationData) => {
					console.log(aceleracion);
					this.aceleracion.x.push(aceleracion.x);
					this.aceleracion.y.push(aceleracion.y);
					this.aceleracion.z.push(aceleracion.z);
				}, e => {
					console.error("Error al obtener aceleracion", e);
				});
		}
	}

	detieneAcelerometro() {
		console.info("Deteniendo lectura de la aceleracion", this.watchAceleracion);
		if(this.watchAceleracion){
			this.watchAceleracion.unsubscribe();
			this.watchAceleracion = undefined;
			this.aceleracion.x.splice(0, this.aceleracion.x.length);
			this.aceleracion.y.splice(0, this.aceleracion.y.length);
			this.aceleracion.z.splice(0, this.aceleracion.z.length);
		}
	}

	getPosicionActual() {
		return this.gps.getCurrentPosition();
	}

	iniciaPosicion() {
		console.info("Inicia lectura de posicion", this.watchGPS);
		var options = { enableHighAccuracy: true };
		if(!this.watchGPS){
			this.watchGPS = this.gps.watchPosition(options).subscribe((data) => {
				var gps = new Gps();
				gps.lat = data.coords.latitude;
				gps.lon = data.coords.longitude;
				this.ruta.push(gps);
			}, e => {
				console.error("Error al obtener gps: ", e);
			});
		}
	}

	detienePosicion() {
		console.info("Deteniendo lectura de posicion", this.watchGPS);
		if(this.watchGPS){
			this.watchGPS.unsubscribe();
			this.watchGPS = undefined;
			this.ruta.splice(0, this.ruta.length);
		}
	}

	iniciaGiroscopio() {
		console.info("Inicia lectura de giroscopio", this.watchGiroscopio);
		var options = { frequency: this.envVariables.girofreq };
		if(!this.watchGiroscopio){
			this.watchGiroscopio = this.giroscopioSensor.watch(options).subscribe(
				(giro: GyroscopeOrientation) => {
					console.log("Giroscopio: ", giro);
	
					this.giroscopio.x.push(giro.x);
					this.giroscopio.y.push(giro.y);
					this.giroscopio.z.push(giro.z);
				}, e => {
					console.error("Error al obtener giroscopio", e);
				});
		}
	}

	detieneGiroscopio() {
		console.info("Detiene lectura de giroscopio", this.watchGiroscopio);
		if(this.watchGiroscopio){
			this.watchGiroscopio.unsubscribe();
			this.watchGiroscopio = undefined;
			this.giroscopio.x.splice(0, this.giroscopio.x.length);
			this.giroscopio.y.splice(0, this.giroscopio.y.length);
			this.giroscopio.z.splice(0, this.giroscopio.z.length);
		}
	}

	customISOstring(date: Date) {
		var date = new Date(date);
		date.setHours(date.getHours() - 4); // apply custom timezone
		function pad(n) { return n < 10 ? '0' + n : n }
		return date.getUTCFullYear() + '-' // return custom format
			+ pad(date.getUTCMonth() + 1) + '-'
			+ pad(date.getUTCDate()) + 'T'
			+ pad(date.getUTCHours()) + ':'
			+ pad(date.getUTCMinutes()) + ':'
			+ pad(date.getUTCSeconds());
	}


}
