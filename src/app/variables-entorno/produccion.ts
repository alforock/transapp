export const prodVariables = {
    apiEndpoint: 'https://transapp-server.herokuapp.com',
    environmentName: 'Entorno de Produccion',
    ionicEnvName: 'prod',
    timeout: 30000,
    accfreq: 3000,
    girofreq: 3000
  };