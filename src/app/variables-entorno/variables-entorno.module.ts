import { NgModule } from '@angular/core';
import { EnvVariables } from './variables-entorno.token';
import { devVariables } from './desarrollo';
import { prodVariables } from './produccion';

declare const process: any; // Typescript compiler will complain without this

export function environmentFactory() {
  console.log("Tipo de Deploy: ", process);

  return process.env.IONIC_ENV === 'prod' ? prodVariables : devVariables;
}

@NgModule({
  providers: [
    {
      provide: EnvVariables,
      // useFactory instead of useValue so we can easily add more logic as needed.
      useFactory: environmentFactory
    }
  ]
})
export class EnvironmentsModule {}